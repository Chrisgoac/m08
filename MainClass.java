public class Salida
{
    public static void main(String[] args)
    {
        System.out.println("Hola usuario bienvenido");

        String dia = "Lunes";
        System.out.println("Hoy es " + dia); //Se componen mensajes usando el + (concatenar)

        int hora = 12;

        System.out.println("Son las " + hora + " en punto");

        System.out.println("Dentro de 2 horas serán las " + hora + 2 + " en punto");//Esta línea está mal

        System.out.println("Dentro de 2 horas serán las " + (hora + 2) + " en punto");//Esta línea si muestra el resultado de 12 + 2
    }
}
